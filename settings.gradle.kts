dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        repositories {
            maven {
                url = uri("https://gitlab.com/api/v4/groups/hadaly/-/packages/maven")
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Private-Token"
                    value = System.getenv("PUBLISH_TOKEN")
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
    versionCatalogs {
        create("libraries") {
            from("fr.hadaly.catalog:kmm:1.0.1")
        }
    }
}
rootProject.name = "detekt-rules"

