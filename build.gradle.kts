import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask
import org.jetbrains.changelog.date
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.20"
    `maven-publish`
    alias(libraries.plugins.detekt)
    alias(libraries.plugins.changelog)
    alias(libraries.plugins.androidGitVersion)
}

group = "fr.hadaly"
version = androidGitVersion.name()

detekt {
    buildUponDefaultConfig = true // preconfigure defaults
    allRules = false // activate all available (even unstable) rules.
    config = files("$projectDir/config/detekt.yml", "$projectDir/config/compose.yml") // point to your custom config defining rules to run, overwriting default behavior
    baseline = file("$projectDir/config/baseline.xml") // a way of suppressing issues before introducing detekt
}

repositories {
    mavenCentral()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Detekt>().configureEach {
    jvmTarget = "1.8"
}
tasks.withType<DetektCreateBaselineTask>().configureEach {
    jvmTarget = "1.8"
}

publishing {
    publications {
        create<MavenPublication>("DetektConfig") {
            groupId = "fr.hadaly.detekt-config"
            artifactId = "detekt"
            version = androidGitVersion.name()
            artifact(projectDir.resolve("config/detekt/detekt.yml"))
        }
        create<MavenPublication>("DetektComposeConfig") {
            groupId = "fr.hadaly.detekt-config"
            artifactId = "compose"
            version = androidGitVersion.name()
            artifact(projectDir.resolve("config/detekt/compose.yml"))
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/40518121/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = System.getenv("PUBLISH_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

changelog {
    version.set("1.0.0")
    path.set("${project.projectDir}/CHANGELOG.md")
    header.set(provider { "[${version.get()}] - ${date()}" })
    itemPrefix.set("-")
    keepUnreleasedSection.set(true)
    unreleasedTerm.set("[Unreleased]")
    groups.set(listOf("Added", "Changed", "Deprecated", "Removed", "Fixed", "Security"))
}
